Mock-server
===

The mock-server is able to run as a stand-alone app.

## Running the mock-server

#### Configuration

By default, the mock-server runs on `http://localhost:9100`. You can configure the port by modifying the `package.json` file:

- `config.port`: the mock-server will run on this port `http://localhost:[config.port]/`

## Folder structure

- `mocks`: contains a folder structure that represent resource_uris

Example:

The request for

    /search/api/v1/search/

will be parsed to

    + /
      + mocks
        + search
          + api
            + v1
              + search
                - index.js (a function that returns a JSON object)

## Writing a mock

Based on the above example

#### Step 1:

Create the folder structure that represent an endpoint

#### Step 2:

Create an `index.js` file - (files named `index.js` will be picked up as modules by the NodeJS module manager)

#### Step 3:

Write the actual mock:

    var equal = require('deep-equal');

    module.exports = function (query, req, res) {
        return {
            a: 'a'
        };
    };

where the arguments for the function are:

- `query`: the URL params as an object.
Ex: `/search/api/v1/search/?q=hsbc&sort=asc` will become `{ q: 'hsbc', sort: 'asc' }`
- `req`: See [ExpressJS - Request](http://expressjs.com/4x/api.html#request)
- `res`: See [ExpressJS - Response](http://expressjs.com/4x/api.html#response)

The above solution allows you to return different JSON objects based on different query arguments.

Example mock for `/example/api/v1/search/?sort=asc` *(assuming that we have `ascMock.json`, `descMock.json` and `filtered.json` files in the same folder as the `index.js` is)*:

##### mocks/example/api/v1/search/index.js

    var equal = require('deep-equal');

    var mocks = [{
        params: { 'filter': 'mediaType:video' },
        mock: require('./filtered.json')
    }, {
        params: { sort: 'asc' },
        mock: require('./ascMock.json')
    }, {
        params: { sort: 'desc' },
        mock: require('./descMock.json')
    }];

    module.exports = function (query, req, res) {

        var filtered = mocks.filter(function(mock){
            return equal(mock.params, query);
        });

        return filtered[0].mock;
    };

This mock allows for multiple different responses based on query params without having much logic. This file can be
found at

## !!! Very important !!!

Mocks should not contain any logic at all, so please, keep it simple!!!

## Example project

Examples of creating mocks can be found in the `example` app mocks in `mocks/example`