var server = require('./server');
var pkg = require('./package.json');

server.listen(pkg.config.port || 9100);