var express = require("express");
var app = express();

var root = "./mocks";

app.all('/*', function(req, res){
    var filePath = root + req.url.split('?')[0];
    var value;
    var mock;

    try {
        mock = require(filePath);
        value = mock(req.query, req, res);
    } catch (e) {
        value = {
            message: e.message
        };

        res.status(404);
    }

    res.send(value);
});

module.exports = app;