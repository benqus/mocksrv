var equal = require('deep-equal');

var mocks = [{
    params: { 'filter': 'mediaType:video' },
    mock: require('./filtered.json')
}, {
    params: { sort: 'asc' },
    mock: require('./ascMock.json')
}, {
    params: { sort: 'desc' },
    mock: require('./descMock.json')
}];

module.exports = function (query) {

    var filtered = mocks.filter(function(mock){
        return equal(mock.params, query);
    });

    return filtered[0].mock;
};